<?php

    class transport {
        private $type;

        public function __constructor($type) {
            $this->type = $type;
        }

        public function setTip($type) {
            $this->type = $type;
        }

        public function getTip() {
            return $this->type;
        }

        public function isMoving($moving){
            if ($moving) {
                echo "It is moving";
            }else {
                echo "It is not moving";
            }
        }
    }