<?php

    require('transport.class.php');

    class train extends transport {
        private $number;
        
        public function __contructor($number) {
            $this->number = $number;
        }

        public function setNumber($number) {
            $this->number = $number;
        }

        public function getNumber() {
            return $this->number;
        }

        public function canStart($full) {
            if ($full) {
                echo "Train can leave the station";
            }else {
                echo "It is not yet the hour to leave the station";
            }
        }
    }