<?php

    require('vehicle.class.php');

    class car extends vehicle{
        private $company;
        
        public function __contructor($company) {
            $this->company = $company;
        }

        public function setCompany($company) {
            $this->company = $company;
        }

        public function getCompany() {
            return $this->company;
        }
    }