<?php

    require('transport.class.php');

    class plain extends transport {
        private $model;
        
        public function __contructor($model) {
            $this->model = $model;
        }

        public function setModel($model) {
            $this->model = $model;
        }

        public function getModel() {
            return $this->model;
        }

        public function takeOff($close){
            if ($close) {
                echo "The gate is closed the plain can take off";
            }else {
                echo "The gate is not closed";
            }
        }
    }