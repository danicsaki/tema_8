<?php

    require('transport.class.php');

    class vehicle extends transport {
        private $name;
        
        public function __contructor($name) {
            $this->name = $name;
        }

        public function setName($name) {
            $this->name = $name;
        }

        public function getName() {
            return $this->name;
        }

        public function hasDriver($driver){
            if ($driver) {
                echo "The driver is in the vehicle";
            }else {
                echo "The vehicle is empty";
            }
        }
    }