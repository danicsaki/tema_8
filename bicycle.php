<?php

    require('vehicle.class.php');

    class bicycle extends vehicle{
        private $code;
        
        public function __contructor($code) {
            $this->code = $code;
        }

        public function setCode($code) {
            $this->code = $code;
        }

        public function getCode() {
            return $this->code;
        }

        public function hasHelmet($helmet){
            if ($helmet) {
                echo "It is safe to rive";
            }else {
                echo "Put on helmet";
            }
        }
    }